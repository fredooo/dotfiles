"
" .vimrc
"

" Use imporved vi
set nocompatible

" Use Vundle as plugin manager
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'

" All used plugins
Plugin 'eagletmt/neco-ghc'
Plugin 'airblade/vim-gitgutter'
Plugin 'dag/vim2hs'
Plugin 'honza/vim-snippets'
Plugin 'jiangmiao/auto-pairs'
Plugin 'kien/ctrlp.vim'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'majutsushi/tagbar'
Plugin 'rhysd/vim-clang-format'
Plugin 'sjl/gundo.vim'
Plugin 'SirVer/ultisnips'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-vividchalk'
Plugin 'Valloric/YouCompleteMe'
Plugin 'xterm-color-table.vim'

call vundle#end()
filetype plugin indent on

"
" General editor configuration
"

" Look and behavior
syntax on
set background=dark
colorscheme vividchalk

set number
set relativenumber

set ruler
set showmode
set showcmd
set laststatus=2
set completeopt=menu

set history=1000
set wildmenu
set wildmode=list:longest

set listchars=tab:▸\ ,trail:·,eol:¶,extends:>,precedes:<,nbsp:+
set backspace=2
set display=lastline

set timeout
set timeoutlen=1000
set ttimeout
set ttimeoutlen=100

set nowrap
set scrolloff=1
set sidescrolloff=5

set nomodeline
set clipboard=unnamed
set visualbell
set lazyredraw
set shortmess+=I

" Settings for graphical vim versions
set guifont=DejaVu\ Sans\ Mono:h12
set guioptions-=r

" Folding options
set nofoldenable

" Search options
set hlsearch
set ignorecase
set smartcase

" File related options
set autoread
set hidden
set encoding=utf-8
set ffs=unix,dos,mac

" Backup options
set nobackup
set nowritebackup
set noswapfile

" Indentation
set tabstop=4
set shiftwidth=4
set shiftround
set softtabstop=4
set expandtab
set autoindent
set smartindent
set smarttab

" Statusbar
set statusline=%t\ %m%r%h\ [%{&ff}]\ %y%=%c,%l/%L\ %p%%
set statusline+=\ %#error#%{SyntasticStatuslineFlag()}%*

" Only highlight lines with over 80 characters
highlight ColorColumn ctermbg=red
call matchadd('ColorColumn', '\%81v', 100)

" Filetype specific omnicomplete configuration
autocmd FileType haskell set omnifunc=necoghc#omnifunc

"
" Key mapping
"

let mapleader = ','

" Unbind the cursor keys in insert, normal and visual mode
for prefix in ['i', 'n', 'v']
    for key in ['<Up>', '<Down>', '<Left>', '<Right>']
        exe prefix . "noremap " . key . " <Nop>"
    endfor
endfor

" Exit insert mode with kj
inoremap kj <esc>
inoremap <esc> <nop>

" Quick .vimrc access
nnoremap <leader>ve :vsplit $MYVIMRC<cr>
nnoremap <leader>vs :source $MYVIMRC<cr>

" Window navigation
nnoremap <leader>w <C-w><C-w>
nnoremap <leader>j <C-w>j
nnoremap <leader>k <C-w>k
nnoremap <leader>h <C-w>h
nnoremap <leader>l <C-w>l
nnoremap <leader>y <C-w>v<C-w>l
nnoremap <leader>x <C-w>s<C-w>j
nnoremap <leader>d <C-w>4< 
nnoremap <leader>s <C-w>4> 
nnoremap <leader>q <C-w>2+ 
nnoremap <leader>a <C-w>2- 

" Tab navigation
nnoremap <leader>. :tabnew<CR>
nnoremap <leader><leader> :tabnext<CR>
nnoremap <leader>m :tabprevious<CR>
nnoremap <leader>- :tabclose<CR>
nnoremap <leader>o :tabonly<CR>
nnoremap <leader>n :tabmove

" Mappings for Syntastic
nnoremap <leader>e :SyntasticCheck<CR>
nnoremap <leader>eo :Errors<CR>
nnoremap <leader>ec :lclose<CR>

" vim-clang-format
autocmd FileType c,cpp,objc nnoremap <Leader>f :ClangFormat<CR>

" Mappings for F1-F12
nnoremap <F2> :NERDTreeToggle<CR>
nnoremap <F3> :TagbarToggle<CR>
nnoremap <F4> :GundoToggle<CR>
nnoremap <F5> :IndentGuidesToggle<CR>
nnoremap <F6> :set list!<CR>
nnoremap <silent> <F7> :call <SID>StripTrailingWhitespaces()<CR>
nnoremap <silent> <F8> :set relativenumber!<CR>

"
" Abbreviations
"

iabbrev @@g fdennig@gmx.net
iabbrev @@u frederik.dennig@uni-konstanz.de
iabbrev ccopy Copyright 2014 Frederik Dennig, all rights reserved.

"
" Custom functions
"

" Key to strip trailing whitespaces
function! <SID>StripTrailingWhitespaces()
  let l = line(".")
  let c = col(".")
  %s/\s\+$//e
  call cursor(l, c)
endfunction

let g:os=substitute(system('uname'), '\n', '', '')

"
" Plugin configuration
"

" YouCompleteMe
if os == "Darwin"
    let g:ycm_global_ycm_extra_conf = '/Users/frederikdennig/.ycm_extra_conf.py'
else
    let g:ycm_global_ycm_extra_conf = '/home/fredo/.ycm_extra_conf.py'
endif
let g:ycm_register_as_syntastic_checker = 0
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_complete_in_comments = 1
let g:ycm_cache_omnifunc = 0
let g:ycm_semantic_triggers = {
    \ 'haskell' : [ '.' ]
\ }

" Syntastic
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = '-Wall -Wextra -Wunreachable-code -std=c++11 -x c++'
let g:syntastic_cpp_config_file = '.syntastic.conf'
let g:syntastic_cpp_check_header = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_enable_balloons = 0
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_loc_list_height = 7
let g:syntastic_stl_format = '[%E{Err: %fe #%e}%B{, }%W{Warn: %fw #%w}]'
let g:syntastic_error_symbol = 'EE'
let g:syntastic_style_error = 'SE'
let g:syntastic_warning_symbol = 'WW'
let g:syntastic_style_warning_symbol = 'SW'
highlight SignColumn ctermbg=Black
highlight SyntasticErrorSign ctermfg=White ctermbg=DarkRed
highlight SyntasticWarningSign ctermfg=White ctermbg=DarkBlue
highlight SyntasticError ctermfg=White ctermbg=DarkRed
highlight SyntasticWarning ctermfg=White ctermbg=DarkBlue

" Indent Guides
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :highlight IndentGuidesOdd ctermbg=233
autocmd VimEnter,Colorscheme * :highlight IndentGuidesEven ctermbg=234

" UltiSnips
let g:UltiSnipsExpandTrigger = '<C-j>'
let g:UltiSnipsJumpForwardTrigger = '<C-j>'
let g:UltiSnipsJumpBackwardTrigger = '<C-k>'
let g:UltiSnipsEditSplit = 'vertical'

" vim2hs
" No concealing
let g:haskell_conceal_wide = 0
let g:haskell_conceal = 0
let g:haskell_conceal_enumerations = 0

" Tagbar
" Haskell support
let g:tagbar_type_haskell = {
    \ 'ctagsbin'  : 'hasktags',
    \ 'ctagsargs' : '-x -c -o-',
    \ 'kinds'     : [
        \ 'm:modules:0:1',
        \ 'd:data: 0:1',
        \ 'd_gadt: data gadt:0:1',
        \ 't:type names:0:1',
        \ 'nt:new types:0:1',
        \ 'c:classes:0:1',
        \ 'cons:constructors:1:1',
        \ 'c_gadt:constructor gadt:1:1',
        \ 'c_a:constructor accessors:1:1',
        \ 'ft:function types:1:1',
        \ 'fi:function implementations:0:1',
        \ 'o:others:0:1'
    \ ],
    \ 'sro'        : '.',
    \ 'kind2scope' : {
        \ 'm' : 'module',
        \ 'c' : 'class',
        \ 'd' : 'data',
        \ 't' : 'type'
    \ },
    \ 'scope2kind' : {
        \ 'module' : 'm',
        \ 'class'  : 'c',
        \ 'data'   : 'd',
        \ 'type'   : 't'
    \ }
\ }

