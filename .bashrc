#
# .bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#
# Prompt
#

if [ $(whoami) = 'root' ]; then
    PS1='\[\e[1;31m\][\u@\h \W]\$\[\e[0m\] '   # Red prompt
else
    PS1='\[\e[1;32m\][\u@\h \W]\$\[\e[0m\] '   # Green prompt
fi

#
# Aliases
#

# Aliases simple terminal shortcuts
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

if [ `uname` == 'Darwin' ]; then
    alias l='ls'
    alias la='ls'
    alias ll='ls -hl'
    alias lla='ls -ahl'
else
    alias ls='ls --color=auto'
    alias l='ls --color=auto'
    alias la='ls --color=auto -a'
    alias ll='ls --color=auto -hl'
    alias lla='ls --color=auto -ahl'
fi

alias df='df -ahT'
alias du='du -h'
alias ds='du -hs'

alias .='pwd'
alias cd..='cd ..'
alias ..='cd ..'
alias ....='cd ../..'
alias ......='cd ../../..'

alias a='alias'
alias c='clear'
alias h='cd ~ && clear'
alias e='exit'

alias halt='systemctl poweroff'
alias suspend='systemctl suspend'
alias hibernate='systemctl hibernate'

alias sv='sudo vim'
alias sn='sudo nano'
alias hs='history | grep'

alias ta='tmux attach'
alias td='tmux detach'

alias vi='vim'
alias ping='ping -c 5'
alias kill='kill -9'
alias lj='java -jar'
alias freeswap='sudo swapoff -a && sudo swapon -a '

alias hc='herbstclient'
alias hlwm='herbstluftwm'

alias stud='cd ~/Dropbox/Studium/Semester_10'

# Aliases for package-managment and system-maintenance
alias sudo='sudo '
alias pacsyn='sudo pacman -Syu'
alias pacins='sudo pacman -S'
alias pacusr='sudo pacman -U'
alias pacchk='pacman -Ss'
alias pacrem='sudo pacman -Rs'
alias pacrip='sudo pacman -Rdd'
alias pacs='pacman -Q'
alias pacaur='pacman -Qm'
alias pacnouse='sudo pacman -Rsn `pacman -Qdtq`'
alias pacopt='sudo pacman -Syy && sudo pacman-optimize'
alias pacclean='sudo pacman -Scc'
alias pacnew='sudo find / -name *.pacnew'

alias aursyn='packer -Syu --auronly'
alias aurins='packer -S --auronly '

# Other aliases
alias matrix='cmatrix -ab -u 3 -C green'
alias cytrix='cmatrix -ab -u 3 -C cyan'
alias retrix='cmatrix -ab -u 3 -C red'

#
# Functions
#

function extract {
    if [ -z "$1" ]; then
        cat << EOF
Usage: extract [file]

Supported extensions:
    zip, rar, bz2, gz, tar, tbz2, tgz, Z, 7z, xz, exe, tar.bz2, tar.gz, tar.xz
EOF
    else
        if [ -f "$1" ]; then
            case "$1" in
                *.tar.bz2)
                    tar xvjf ./"$1"
                    ;;
                *.tar.gz)
                    tar xvzf ./"$1"
                    ;;
                *.tar.xz)
                    tar xvJf ./"$1"
                    ;;
                *.lzma)
                    unlzma ./"$1"
                    ;;
                *.bz2)
                    bunzip2 ./"$1"
                    ;;
                *.rar)
                    unrar x -ad ./"$1"
                    ;;
                *.gz)
                    gunzip ./"$1" 
                    ;;
                *.tar)      
                    tar xvf ./"$1"     
                    ;;
                *.tbz2)     
                    tar xvjf ./"$1"   
                    ;;
                *.tgz)      
                    tar xvzf ./"$1"   
                    ;;
                *.zip)      
                    unzip ./"$1"      
                    ;;
                *.Z)        
                    uncompress ./"$1" 
                    ;;
                *.7z)       
                    7z x ./"$1"       
                    ;;
                *.xz)       
                    unxz ./"$1"       
                    ;;
                *.exe)      
                    cabextract ./"$1" 
                    ;;
                *)
                    echo "extract: '$1' has unknown file extension" 
                    ;;
            esac    
        else
            echo "extract: '$1' does not exist"
        fi
    fi
}

function take {
    if [ -z "$1" ]; then
        echo "take: no name or path specified"
    else
        mkdir -p ./"$1" && cd ./"$1"
    fi
}

#
# Variables
#

# Default editor: vim
export EDITOR='vim'

# Default browser: chromium
export BROWSER='chromium'

# Colored ls output on Mac
if [ `uname` == 'Darwin' ]; then
    export CLICOLOR=1
    export LSCOLORS=ExGxBxDxCxEgEdxbxgxcxd
fi

# Colored ls output for the termite terminal
if [ $TERM == 'xterm-termite' ]; then
    eval $(dircolors ~/.dircolors)
fi
